﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;

namespace ConsoleApp1
{
    class Departments

    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Id + " " + Name;
        }
    }


    public class Program
    {
        private static readonly HttpClient _Client = new HttpClient();

        static void Main(string[] args)
        {
            Run().Wait();
        }

        static async Task Run()
        {
            string url = "http://api.dekanat.oa.edu.ua/faculties";
            var response = await _Client.GetAsync(url);
            string responseText = await response.Content.ReadAsStringAsync();
            List<Departments> abs = JsonConvert.DeserializeObject<List<Departments>>(responseText);

            Console.WriteLine(responseText);
            Console.WriteLine();
            Console.WriteLine(abs.ToString());
            Console.ReadLine();
        }
    }
}
